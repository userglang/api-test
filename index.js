const express = require('express');
const cors = require("cors");

const userRoutes = require("./routes/userRoutes") 

const app = express();

const connection = require("./connection");


app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));


app.use("/user", userRoutes);

module.exports = app;