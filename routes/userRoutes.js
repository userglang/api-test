
const express = require("express");
const router = express.Router();
const userControllers = require("../controllers/userControllers");
const auth = require("../auth");

// User

// Admin
// Get all user information
router.get("/", auth.verify, userControllers.getAlluser)
// Archive specific user 
router.patch("/archive/:id", auth.verify, userControllers.archiveUser)
// Delete specific user information
router.delete("/removeAccount/:id", auth.verify, userControllers.removeUser)
// Set user account type
router.patch("/setAccount/:id", auth.verify, userControllers.setUser)

// Both (User and Admin)
// Get specific user information 
router.get("/accountInfo", auth.verify, userControllers.userInfo)
// Check user email duplication 
router.get("/checkEmail", userControllers.emailDuplicate)
// Create user account information
router.post("/create", userControllers.createUser)
// login account
router.post("/login", userControllers.loginUser)
// Update personal/specific user information
router.patch("/update/:id", userControllers.updateInfo)


module.exports = router;