const jwt = require("jsonwebtoken");

const secret =  "2444666668888888";

module.exports.createAccessToken = (user) =>{
	// When the user logs in, a token will be created with the user's information.
	// This will be used for the token payload
	const data = {
		userId: user.userId,
		email: user.email,
		isActive: user.isActive,
		isAdmin: user.isAdmin
	}
	// Generate a JSON web token using the jwt's sign method
	return jwt.sign(data, secret, {});
}


// Middleware functions
module.exports.verify = (req, res, next) =>{
	let token = req.headers.authorization;
	//console.log(typeof token);
	if(typeof token !== "undefined"){
		// This removes the "Bearer " prefix and obtains only the token for verification.
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return res.send({auth: "Token Failed"});
			}
			else{
				next();
			}
		})
	}
	else{
		return res.send({auth: "Failed"});
	}
}


module.exports.decode = (token) =>{
	if(typeof token !== "undefined"){
		token = token.slice(7, token.length);
		return jwt.verify(token, secret, (err, data)=>{
			// If JWT is not valid
			if(err){
				return null;
			}
			else{
				return jwt.decode(token, {complete: true}).payload;
			}
		})
	}
	else{
		return null;
	}
}
