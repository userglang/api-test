const bcrypt = require("bcrypt");
const auth = require("../auth");
const connection = require("../connection");


// Get all user information
module.exports.getAlluser = (req, res) => {

    const userData = auth.decode(req.headers.authorization); //contains the token
    let getUserTableQuery = "Select * from user_table";

    if(userData.isAdmin && userData.isActive){
        connection.query(getUserTableQuery,(err, result)=>{
            if(!err){
                res.send(result);
            }
            else{
                res.send(false);
            }
        })
    }
    else{
        console.log("Failed")
        res.send(false)
    }
    
}

// Archive specific user 
module.exports.archiveUser = (req, res, next) => {
    
    const userData = auth.decode(req.headers.authorization); //contains the token

    let inputData = req.body;
    const userId = req.params.id;
    let archiveUserQuery = "UPDATE `user_table` SET `isActive`=? WHERE `userId`=?";
    
    if(userData.isAdmin && userData.isActive){
        connection.query(archiveUserQuery,[inputData.isActive, userId],(err, result)=>{
            if(!err){
                res.send(true);
            }
            else{
                console.log(err)
                res.send(false);
            }
        })
    }
    else {
        console.log("Failed")
        res.send(false)
    }
}

// Delete specific user information
module.exports.removeUser = (req, res, next) => {
    
    const userData = auth.decode(req.headers.authorization); //contains the token

    const userId = req.params.id;
    let removeUserQuery = "DELETE FROM `user_table` WHERE `userId`=?";
    
    if(userData.isAdmin && userData.isActive){
        connection.query(removeUserQuery,[userId],(err, result)=>{
            if(!err){
                console.log("User Successfully Deleted.")
                res.send(true);
            }
            else{
                console.log(err)
                res.send(false);
            }
        })
    }
    else {
        console.log("Failed")
        res.send(false)
    }
    
}

// Set user account type
module.exports.setUser = (req, res, next) => {
    
    const userData = auth.decode(req.headers.authorization); //contains the token

    let inputData = req.body;
    const userId = req.params.id;
    let setUserQuery = "UPDATE `user_table` SET `isAdmin`=? WHERE `userId`=? AND ";

    if(userData.isAdmin && userData.isActive){
        connection.query(setUserQuery,[inputData.isAdmin, userId],(err, result)=>{
            if(!err){
                res.send(true);
            }
            else{
                console.log(err)
                res.send(false);
            }
        })
    }
    else{
        console.log("Failed")
        res.send(false)
    }
}

// Get specific user information 
module.exports.userInfo =  (req, res) => {

    const userData = auth.decode(req.headers.authorization); //contains the token

    let userInfoQuery = "SELECT * FROM `user_table` WHERE `userId`=?";
    
    if(userData.isActive){
        connection.query(userInfoQuery,[userData.userId],(err, queryResult)=>{
            const result = Object.values(JSON.parse(JSON.stringify(queryResult)))
    
            if(result.length > 0){
                res.send(result[0]);
            }
            else{
                console.log(err)
                res.send(false);
            }
        })
    }
    else{
        console.log("Sorry, this account account was currently deactivated!")
        res.send(false)
    }
}

// Check user email duplication
module.exports.emailDuplicate = (req, res, next) => {
    let inputData = req.body;
    let emailDuplicateQuery = "SELECT `email` FROM `user_table` WHERE `email`=?";
    connection.query(emailDuplicateQuery,[inputData.email],(err, result)=>{
        if(result.length > 0){
            res.send(true);
        }
        else{
            console.log(err)
            res.send(false);
        }
    })
}

// Create user account information
module.exports.createUser = (req, res, next) => {
    let inputData = req.body;
    let passcode = bcrypt.hashSync(req.body.password, 10)
    let createUserQuery = "INSERT INTO `user_table`(`userId`, `firstName`, `lastName`, `email`, `password`, `dateCreated`) VALUES (?,?,?,?,?,?)";
    let emailDuplicateQuery = "SELECT `email` FROM `user_table` WHERE `email`=?";
    connection.query(emailDuplicateQuery,[inputData.email],(err, selectResult)=>{
        const result = Object.values(JSON.parse(JSON.stringify(selectResult)))
        if(result.length > 0){
            console.log("Email already exist")
            res.send(false)
        }
        else{
            connection.query(createUserQuery,[inputData.userId, inputData.firstName, inputData.lastName, inputData.email, passcode, inputData.dateCreated],(err, insertResult)=>{
                if(!err){
                    console.log("User Successfully added")
                    res.send(true);
                }
                else{
                    console.log(err)
                    res.send(false);
                }
            })
        }
    })
}

// login account
module.exports.loginUser = (req, res, next) => {
    let inputData = req.body;
    let loginUserQuery = "SELECT * FROM `user_table` WHERE `email`=?";
    connection.query(loginUserQuery,[inputData.email],(err, selectResult)=>{
        const result = Object.values(JSON.parse(JSON.stringify(selectResult)))
        if(result.length === 0){
            console.log(`Incorrect email.`)
            res.send(false);
        }
        else{
			// Syntax: bcrypt.compareSync(data, encrypted);
			// bcrypt.compareSync() return Boolean
			const isPasswordCorrect = bcrypt.compareSync(inputData.password, result[0].password);
			// If the passwords match the result.
			if(isPasswordCorrect){
				res.send({access: auth.createAccessToken(result[0])})
			}
			// Password do not match
			else{
				res.send(false);
			}
        }
    })
}

// Update personal/specific user information
module.exports.updateInfo = (req, res, next) => {
    let inputData = req.body;
    const userId = req.params.id;
    let updateUserQuery = "UPDATE `user_table` SET `firstName`=?,`lastName`=?,`email`=?,`password`=? WHERE `userId`=?";
    connection.query(updateUserQuery,[inputData.firstName, inputData.lastName, inputData.email, inputData.password, userId],(err, result)=>{
        if(!err){
            res.send(true);
        }
        else{
            console.log(err)
            res.send(false);
        }
    })
}